# Duomenų žvalgomoji analizė #

vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_1$ wc -l X.txt
2108246 X.txt
vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_1$ wc -l y.txt
2108246 y.txt

Išvados: assignment_1 duomenų rinkinyje yra:
2108246 X 
2108246 y'

vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_2$ wc -l X.txt
2070116 X.txt
vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_2$ wc -l y.txt
2070116 y.txt

Išvados: assignment_2 duomenų rinkinyje yra:
2070116 X
2070116 y

/apjungimas ir skaidymas/

vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_1$ paste -d ' ' y.txt X.txt | mungy split - 0.6,0.2,0.2 --filenames train.txt,validation.txt,test.txt
vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_2$ paste -d ' ' y.txt X.txt | mungy split - 0.6,0.2,0.2 --filenames train.txt,validation.txt,test.txt

/train, validation, test/

vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_1$ mungy csv2vw train.txt --delim=' ' --label=0 --no-binary-label --no-header -o train.vw
vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_1$ mungy csv2vw validation.txt --delim=' ' --label=0 --no-binary-label --no-header -o validation.vw
vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_1$ mungy csv2vw test.txt --delim=' ' --label=0 --no-binary-label --no-header -o test.vw  
vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_2$ mungy csv2vw test.txt --delim=' ' --label=0 --no-binary-label --no-header -o test.vw
vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_2$ mungy csv2vw train.txt --delim=' ' --label=0 --no-binary-label --no-header -o train.vw
vagrant@ktu-mgmf-ubuntu-server-1804:~/synced_dir/project1/data/raw/007/assignment_2$ mungy csv2vw validation.txt --delim=' ' --label=0 --no-binary-label --no-header -o validation.vw
